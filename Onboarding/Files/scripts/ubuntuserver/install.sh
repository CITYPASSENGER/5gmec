#!/bin/bash
sudo echo "127.0.0.1 $HOSTNAME" >> /etc/hosts
echo "Use simple debug messages to understand where the issues could be"
echo "installing!!!"
echo "Avoid long outputs! so redirect the output to a file that you can later check, for instance:"
sudo apt-get update > /tmp/install.log
echo "apt update worked!" 
sudo apt-get install build-essential libexpat-dev libgmp-dev libssl-dev libpcap-dev byacc flex git python-dev python-pastedeploy python-paste python-twisted openvswitch-switch >> /tmp/install.log
echo "Packages installed"
sudo cat > /etc/network/interfaces.d/ens4.cfg << EOF
auto ens4
iface ens4 inet dhcp
echo "Enabling ipv4 forwarding"
sudo sysctl -w net.ipv4.ip_forward=1 
EOF
sudo ifup ens4
