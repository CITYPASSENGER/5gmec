#!/bin/bash

declare -i x=1

while(true);
do

tcpdump -G 5 -W 1 -w signal.pcap -i signaling 

tshark -r signal.pcap -R "nas_eps.nas_msg_emm_type == 0x42" -2 -T json > signal.json

cat signal.json | grep nas_eps.esm.pdn_ipv4 | awk {'print $2'} | sed 's/"*//g' > ue.txt
cat signal.json | grep s1ap.gTP_TEID | awk {'print $2'} | sed 's/"*//g'  | sed 's/:*//g' | sed 's/,*//g' > teid.txt
cat signal.json | grep s1ap.transportLayerAddressIPv4 | awk {'print $2'} | sed 's/"*//g' > sgw.txt
cat signal.json | grep ip.dst_host | awk {'print $2'} | sed 's/"*//g'  | sed 's/:*//g' | sed 's/,*//g' > enb.txt

cat signal.json  | grep s1ap.MME_UE_S1AP_ID | awk {'print $2'} | sed 's/"*//g' > enb_teid_tmp.txt

while IFS='' read -r line || [[ -n "$line" ]]; do
    tshark -2 -r signal.pcap -R "((s1ap.procedureCode == 9) && (s1ap.S1AP_PDU == 1)) && (s1ap.MME_UE_S1AP_ID == $line)" -T json  | grep TEID | awk {'print $2'} | sed 's/"*//g' | sed 's/:*//g' >> enb_teid.txt
done < "enb_teid_tmp.txt"


while IFS='' read -r line || [[ -n "$line" ]]; do
    mac=$(arp -a | grep $line  | awk {'print $4'})
    echo $mac >> enb_mac.txt
done < "enb.txt"

while IFS='' read -r line || [[ -n "$line" ]]; do
    mac=$(arp -a | grep $line  | awk {'print $4'})
    echo $mac >> sgw_mac.txt
done < "sgw.txt"

if [[ -f "ue.txt" && -s "ue.txt" ]]; then 
paste -d"," ue.txt sgw.txt teid.txt enb.txt enb_mac.txt sgw_mac.txt enb_teid.txt > rnis.txt
cat rnis.txt
perl create_app.pl rnis.txt
cat intro_part.txt > mec_app.py
cat encap_part.txt >> mec_app.py
cat decap_part.txt >> mec_app.py
fi

rm -rf signal.json
rm -rf signal.pcap
rm -rf sgw_mac.txt
rm -rf enb_mac.txt

done

