#!/bin/bash 
 
ovs-ofctl -O OpenFlow13 add-flow OVSbr1 in_port=3,table=0,dl_type=0x0800,ip_dst=10.20.16.2,priority=100,actions=mod_dl_src=fa:16:3e:7f:aa:2d,mod_dl_dst=bb:bb:bb:bb:bb:bb,output:5 
ovs-ofctl -O OpenFlow13 add-flow OVSbr1 in_port=3,table=0,dl_type=0x0800,ip_dst=2.0.0.2,priority=100,actions=mod_dl_src=fa:16:3e:7f:aa:2d,mod_dl_dst=aa:aa:aa:aa:aa:aa,output:5 
ovs-ofctl -O OpenFlow13 add-flow OVSbr1 in_port=LOCAL,table=0,dl_type=0x0800,ip_dst=5.144.254.242,tcp,tcp_src=80,priority=100,actions=output:2 
ovs-ofctl -O OpenFlow13 add-flow OVSbr1 in_port=LOCAL,table=0,dl_type=0x0800,ip_dst=5.144.254.242,tcp,tcp_src=3128,priority=100,actions=output:2 
ovs-ofctl -O OpenFlow13 add-flow OVSbr1 in_port=LOCAL,table=0,dl_type=0x0800,ip_dst=10.20.16.2,priority=100,actions=output:2 
ovs-ofctl -O OpenFlow13 add-flow OVSbr1 in_port=3,table=0,dl_type=0x0800,ip_dst=10.20.16.1,priority=100,actions=mod_dl_src=fa:16:3e:7f:aa:2d,mod_dl_dst=dd:dd:dd:dd:dd:dd,output:5 
ovs-ofctl -O OpenFlow13 add-flow OVSbr1 in_port=3,table=0,dl_type=0x0800,ip_dst=2.0.0.2,priority=100,actions=mod_dl_src=fa:16:3e:7f:aa:2d,mod_dl_dst=cc:cc:cc:cc:cc:cc,output:5 
ovs-ofctl -O OpenFlow13 add-flow OVSbr1 in_port=LOCAL,table=0,dl_type=0x0800,ip_dst=10.19.72.140,tcp,tcp_src=80,priority=100,actions=output:2 
ovs-ofctl -O OpenFlow13 add-flow OVSbr1 in_port=LOCAL,table=0,dl_type=0x0800,ip_dst=10.19.72.140,tcp,tcp_src=3128,priority=100,actions=output:2 
ovs-ofctl -O OpenFlow13 add-flow OVSbr1 in_port=LOCAL,table=0,dl_type=0x0800,ip_dst=10.20.16.1,priority=100,actions=output:2 
