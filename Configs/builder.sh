#!/bin/bash
LOCAL_IP=$(ifconfig ens4 | grep Bcast | sed 's/inet addr://g' | awk {'print $1'})
echo $LOCAL_IP
LOCAL_MAC=$(ifconfig ens4 | grep HW | awk {'print $5'})
echo $LOCAL_MAC
apt install pip
pip install setuptools==20.0
pip install nose
pip install webob
pip install tinyrpc
pip install routes
pip install ovs
pip install oslo.config
pip install eventlet
pip install msgpack-python
cd /usr/local/src
modprobe dummy
ip link add dummy1 type dummy
ip link set name port1 dev dummy1
ip link set port1 up
ip link add dummy2 type dummy
ip link set name port2 dev dummy2
ip link set port2 up
ip link add dummy3 type dummy
ip link set name port3 dev dummy3
ip link set port3 up
ip link add dummy4 type dummy
ip link set name signal dev dummy4
ip link set signal up
ifconfig ens4 0
ovs-vsctl add-br OVSbr1
ovs-vsctl set bridge OVSbr1 protocols=OpenFlow13
ovs-vsctl add-port OVSbr1 port1
ovs-vsctl add-port OVSbr1 port2
ovs-vsctl add-port OVSbr1 port3
ovs-vsctl add-port OVSbr1 signal
ovs-vsctl add-port OVSbr1 ens4
ifconfig OVSbr1 $LOCAL_IP/24 up
ovs-ofctl -O OpenFlow13 add-flow OVSbr1 in_port=LOCAL,priority=1,actions=mod_dl_src=$LOCAL_MAC,output:5
iptables -t nat -A PREROUTING -i OVSbr1 -p tcp -m tcp --dport 80 -j DNAT --to-destination $LOCAL_IP:3128 
iptables -t nat -A PREROUTING -i OVSbr1 -p tcp -m tcp --dport 80 -j REDIRECT --to-ports 3128 
iptables -t nat -A POSTROUTING -s $LOCAL_IP -i OVSbr1 -o ens3 -j MASQUERADE