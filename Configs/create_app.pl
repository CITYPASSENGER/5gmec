#!/usr/bin/perl
use strict;
use warnings;
 
my $file = $ARGV[0] or die "Need the csv with UE information \n";

open(my $data, '<', $file) or die "Could not open '$file' $!\n";
open(my $of, '>', 'encap_part.txt');
open(my $ovf, '>', 'ovs_commands.sh');

print $ovf "\#!/bin/bash \n";
print $ovf " \n";

while (my $line = <$data>) {
  chomp $line; 
  my @fields = split "," , $line;
  print $of " ### Ue address: $fields[0] \n";
  print $of " ### Encap flow from ue to sgw \n";
  print $of "        match = parser.OFPMatch(in_port=2, eth_type=2048, ipv4_src='$fields[0]') \n";
  print $of "        actions = [ \n";
  print $of "                  # decap ether \n";
  print $of "                   parser.OFPActionDecap(type_eth, type_ip), \n";
  print $of "                   # encap gtpu \n";
  print $of "                   parser.OFPActionEncap(type_gtpu), \n";
  print $of "                   # set gtpu field \n";
  print $of "                   parser.OFPActionSetField(gtpu_flags=48), \n";
  print $of "                   parser.OFPActionSetField(gtpu_teid=$fields[2]), \n";
  print $of "                   # encap udp \n";
  print $of "                   parser.OFPActionEncap(type_udp), \n";
  print $of "                   # set udp field \n";
  print $of "                   parser.OFPActionSetField(udp_src=2152), \n";
  print $of "                   parser.OFPActionSetField(udp_dst=2152), \n";
  print $of "                   # encap ip \n";
  print $of "                   parser.OFPActionEncap(type_ip), \n";
  print $of "                   # set ip field \n";
  print $of "                   parser.OFPActionSetField(ipv4_src='$fields[3]'), \n";
  print $of "                   parser.OFPActionSetField(ipv4_dst='$fields[1]'), \n";
  print $of "                   parser.OFPActionSetNwTtl(nw_ttl=64), \n";
  print $of "                   # encap ether \n";
  print $of "                   parser.OFPActionEncap(type_eth), \n";
  print $of "                   # set ether field \n";
  print $of "                   parser.OFPActionSetField(eth_src='33:33:33:33:33:33'), \n";
  print $of "                   parser.OFPActionSetField(eth_dst='44:44:44:44:44:44'), \n";
  print $of "                   # output \n";
  print $of "                  parser.OFPActionOutput(3, ofproto.OFPCML_NO_BUFFER) \n";
  print $of "       ] \n";
  print $of "        self.add_flow(datapath, 0, match, actions) \n";
  print $of " \n";

  print $of " ### Ue address: $fields[0] \n";
  print $of " ### Encap flow from sgw to ue \n";
  print $of "        match = parser.OFPMatch(in_port=2, eth_type=2048, ipv4_dst='$fields[0]') \n";
  print $of "        actions = [ \n";
  print $of "                  # decap ether \n";
  print $of "                   parser.OFPActionDecap(type_eth, type_ip), \n";
  print $of "                   # encap gtpu \n";
  print $of "                   parser.OFPActionEncap(type_gtpu), \n";
  print $of "                   # set gtpu field \n";
  print $of "                   parser.OFPActionSetField(gtpu_flags=48), \n";
  print $of "                   parser.OFPActionSetField(gtpu_teid=$fields[6]), \n";
  print $of "                   # encap udp \n";
  print $of "                   parser.OFPActionEncap(type_udp), \n";
  print $of "                   # set udp field \n";
  print $of "                   parser.OFPActionSetField(udp_src=2152), \n";
  print $of "                   parser.OFPActionSetField(udp_dst=2152), \n";
  print $of "                   # encap ip \n";
  print $of "                   parser.OFPActionEncap(type_ip), \n";
  print $of "                   # set ip field \n";
  print $of "                   parser.OFPActionSetField(ipv4_src='$fields[1]'), \n";
  print $of "                   parser.OFPActionSetField(ipv4_dst='$fields[3]'), \n";
  print $of "                   parser.OFPActionSetNwTtl(nw_ttl=64), \n";
  print $of "                   # encap ether \n";
  print $of "                   parser.OFPActionEncap(type_eth), \n";
  print $of "                   # set ether field \n";
  print $of "                   parser.OFPActionSetField(eth_src='33:33:33:33:33:33'), \n";
  print $of "                   parser.OFPActionSetField(eth_dst='44:44:44:44:44:44'), \n";
  print $of "                   # output \n";
  print $of "                  parser.OFPActionOutput(3, ofproto.OFPCML_NO_BUFFER) \n";
  print $of "       ] \n";
  print $of "        self.add_flow(datapath, 0, match, actions) \n";
  print $of " \n";

## Encapsulated packets must be forwarded
  print $ovf "ovs-ofctl -O OpenFlow13 add-flow OVSbr1 in_port=3,table=0,dl_type=0x0800,ip_dst=$fields[1],priority=100,actions=mod_dl_src=$ARGV[2],mod_dl_dst=$fields[5],output:5 \n";
  print $ovf "ovs-ofctl -O OpenFlow13 add-flow OVSbr1 in_port=3,table=0,dl_type=0x0800,ip_dst=$fields[3],priority=100,actions=mod_dl_src=$ARGV[2],mod_dl_dst=$fields[4],output:5 \n";
#  print $ovf "ovs-ofctl -O OpenFlow13 add-flow OVSbr1 in_port=3,table=0,dl_type=0x0800,ip_src=$fields[0],tcp,tcp_dst=80,actions=mod_tp_dst=3212,LOCAL \n";
# From web to ue goes to squid
#  print $ovf "ovs-ofctl -O OpenFlow13 add-flow OVSbr1 in_port=3,table=0,dl_type=0x0800,ip_dst=$fields[0],tcp,tcp_src=80,actions=mod_nw_dst=$ARGV[1],LOCAL \n";

# Tunnel Squid traffic 
# Cached result
  print $ovf "ovs-ofctl -O OpenFlow13 add-flow OVSbr1 in_port=LOCAL,table=0,dl_type=0x0800,ip_dst=$fields[0],tcp,tcp_src=80,priority=100,actions=output:2 \n";
  print $ovf "ovs-ofctl -O OpenFlow13 add-flow OVSbr1 in_port=LOCAL,table=0,dl_type=0x0800,ip_dst=$fields[0],tcp,tcp_src=3128,priority=100,actions=output:2 \n";
  print $ovf "ovs-ofctl -O OpenFlow13 add-flow OVSbr1 in_port=LOCAL,table=0,dl_type=0x0800,ip_dst=$fields[1],priority=100,actions=output:2 \n";

}


close $of;
close $data;
close $ovf;  


