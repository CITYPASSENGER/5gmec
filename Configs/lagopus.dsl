channel channel01 create -dst-addr 127.0.0.1 -protocol tcp

controller controller01 create -channel channel01 -role equal -connection-type main

interface interface01 create -type ethernet-rawsock -device port1
interface interface02 create -type ethernet-rawsock -device port2
interface interface03 create -type ethernet-rawsock -device port3

port port1 create -interface interface01
port port2 create -interface interface02
port port3 create -interface interface03

bridge bridge01 create -controller controller01 -port port1 1 -port port2 2 -port port3 3 -dpid 0x1

bridge bridge01 enable
